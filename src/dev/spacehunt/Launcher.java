package dev.spacehunt;

import dev.spacehunt.engine.Game;

public class Launcher {
	public static void main(String[] args) {
		Game g = new Game("Space Hunt", 1080, 720);
		g.start();
	}
}

package dev.spacehunt.engine;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MouseManager implements MouseListener {
    private int x;
    private int y;

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        /*
        public Rectangle play = new Rectangle(540-80,300,200,90);
        public Rectangle credits = new Rectangle(540-80,400,200,90);
        public Rectangle quit = new Rectangle(540-80,500,200,90);
         */
        x = e.getX();
        y = e.getY();
        if (Game.state == Game.STATE.MENU) {
            if (x >= 460 && x <= 660 && y >= 300 && y <= 390) {
                Game.state = Game.STATE.GAME;
            } else if (x >= 460 && x <= 660 && y >= 500 && y <= 590) {
                System.exit(1);
            } else if (x >= 460 && x <= 660 && y >= 400 && y <= 490) {
                Game.state = Game.STATE.CREDITS;
            }
        }

        if (Game.state == Game.STATE.CREDITS) {
            if (x >= 460 && x <= 660 && y >= 600 && y <= 690) {
                Game.state = Game.STATE.MENU;
            }
        }
        if (Game.state == Game.STATE.DEAD) {
            if (x >= 420 && x <= 620 && y >= 500 && y <= 590) {
                Game.state = Game.STATE.GAME;
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}

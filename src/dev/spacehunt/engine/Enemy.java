package dev.spacehunt.engine;

import dev.spacehunt.gfx.Assets;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

public class Enemy extends Entity {
    private int health;
    private BufferedImage type;

    public Enemy(Handler handler) {
        super(handler);
        int typeID = rnd.nextInt(3);
        if (typeID == 0) {
            type = Assets.enemyShip1;
        } else if (typeID == 1) {
            type = Assets.enemyShip2;
        } else {
            type = Assets.enemyShip3;
        }
        x = rnd.nextInt(1080 - 45);
        y = 0;
    }

    @Override
    protected void render(Graphics g) {
        g.drawImage(Assets.enemyShip1, (int) x, (int) y, null);
    }

    @Override
    public void tick() {
        y++;
    }

    public void randomXMovement() {
        int quantityRandom = rnd.nextInt(5);
        int directionRandom = rnd.nextInt(8);
        if (x + quantityRandom - 43 < 1080) {
            if (directionRandom < 3)
                x += quantityRandom;
            else if (directionRandom < 6)
                x -= quantityRandom;
        }
    }

    public void createBullets() {
        Random rnd = new Random();
        int random = rnd.nextInt(2);
        if (random == 1)
            handler.getGame().addBullets(new Bullet(handler, x + 15, y + 34, 'D'));
    }

    @Override
    protected Rectangle bounds() {
        return new Rectangle((int) x, (int) y, 35, 35);
    }

    public boolean checkCollision(Ship ship) {
        if (this.bounds().intersects(ship.bounds())) {
            System.out.println("bam!");
            ship.setHealth(ship.getHealth() - 10);
            return true;
        }
        return false;
    }

}

package dev.spacehunt.engine;

import dev.spacehunt.gfx.Assets;

import java.awt.*;

public class PowerUp extends Entity {

    public int type;
    protected float x, y;


    public PowerUp(Handler handler, float x, float y, int type) {
        super(handler, x, y);
        this.x = x;
        this.type = type;
    }

    protected void effect(Ship ship) {
        switch (type) {
            case 0:
                ship.setHealth(ship.getHealth() + 10);
                break;
            case 1:
                ship.setPoint(ship.getPoint() + 100);
                break;
            case 2:
                ship.setSpeed(ship.getSpeed() + 0.5f);
                break;
        }

    }

    public boolean checkCollision(Ship ship) {
        if (this.bounds().intersects(ship.bounds())) {
            return true;
        }
        return false;
    }

    @Override
    protected void render(Graphics g) {
        switch (type) {
            case 0:
                g.drawImage(Assets.healthUp, (int) x, (int) y, 32, 32, null);
                break;
            case 1:
                g.drawImage(Assets.pointUp, (int) x, (int) y, 32, 32, null);
                break;
            case 2:
                g.drawImage(Assets.speedUp, (int) x, (int) y, 32, 32, null);
                break;
        }
    }

    @Override
    protected void tick() {
        this.y++;
    }

    @Override
    protected Rectangle bounds() {
        return new Rectangle((int) x, (int) y, 32, 32);
    }
}

package dev.spacehunt.engine;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public abstract class Entity {
	protected float x, y;
	protected Handler handler;
	protected static Random rnd = new Random();

	public Entity(Handler handler, float x, float y) {
		this.handler = handler;
		this.x = x;
		this.y = y;
	}

	public Entity(Handler handler) {
		this.handler = handler;
	}

	protected abstract void render(Graphics g);

	protected abstract void tick();

	protected abstract Rectangle bounds();

}

package dev.spacehunt.engine;

import dev.spacehunt.gfx.Assets;

import java.awt.*;

public class Menu {
    public Rectangle play = new Rectangle(540-80,300,200,90);
    public Rectangle credits = new Rectangle(540-80,400,200,90);
    public Rectangle quit = new Rectangle(540-80,500,200,90);
    public Rectangle menu = new Rectangle(540-80,600,200,90);
    public Rectangle playAgain = new Rectangle(500-80,500,200,90);


    public void render(Graphics g){
        if (Game.state == Game.STATE.MENU){
            g.setFont(new Font("Hack",Font.BOLD,100));
            g.setColor(Color.DARK_GRAY);
            g.drawImage(Assets.menu,0,0,1080,720,null);
            g.drawString("SPACE HUNT", 260,150);
            Graphics2D g2d = (Graphics2D)g;
            g2d.draw(play);
            g2d.draw(credits);
            g2d.draw(quit);
            g.setFont(new Font("Hack",Font.PLAIN,40));
            g.setColor(Color.BLACK);
            g.drawString("PLAY",510,360);
            g.drawString("CREDITS",480,460);
            g.drawString("QUIT",510,560);
        }else if (Game.state == Game.STATE.CREDITS){
            g.setFont(new Font("Hack",Font.BOLD,80));
            g.setColor(Color.DARK_GRAY);
            g.drawImage(Assets.menu,0,0,1080,720,null);
            g.drawString("SERHAT YUNA",250,300);
            g.drawString("MUHAMMET ÖZDAMAR",150,400);
            g.drawString("2018",450,500);
            Graphics2D g2d = (Graphics2D)g;
            g2d.draw(menu);
            g.setFont(new Font("Hack",Font.PLAIN,40));
            g.setColor(Color.BLACK);
            g.drawString("MENU",510,660);
        }else if(Game.state == Game.STATE.DEAD){
            g.setFont(new Font("Hack",Font.BOLD,60));
            g.setColor(Color.RED);
            g.drawImage(Assets.menu,0,0,1080,720,null);
            g.drawString("YOU ARE DEAD!", 280,300);
            Graphics2D g2d = (Graphics2D)g;
            g2d.draw(playAgain);
            g.drawString("PLAY",450,560);


        }


    }






}

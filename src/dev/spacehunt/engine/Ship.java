package dev.spacehunt.engine;

import java.time.Duration;
import java.time.Instant;

import dev.spacehunt.gfx.Assets;

import java.awt.*;
import java.util.ArrayList;

public class Ship extends Entity {
	private int point;
	private int health;
	private float speed = 3.0f;
	private Instant start;

	public Ship(Handler handler, float x, float y) {
		super(handler, x, y);
		point = 0;
		health = 100;
	}

	@Override
	protected void render(Graphics g) {
		g.drawImage(Assets.playerShip, (int) x, (int) y, null);
	}

	@Override
	protected void tick() {
		if (handler.getKeyManager().right) {
			if (x + speed + 59 < 1080)
				x += speed;
		}
		if (handler.getKeyManager().left) {
			if (x - speed > 0)
				x -= speed;
		}
		if (handler.getKeyManager().down) {
			if (y + speed + 42 < 720)
				y += speed;
		}
		if (handler.getKeyManager().up) {
			if (y - speed > 0)
				y -= speed;
		}
		if (handler.getKeyManager().attack) {
			if (start == null) {
				handler.getGame().addBullets(new Bullet(handler, this.x + 26, this.y, 'U'));
				start = Instant.now();
			} else {
				Instant end = Instant.now();
				Duration timeDifference = Duration.between(start, end);
				if (timeDifference.toMillis() >= 500) {
					handler.getGame().addBullets(new Bullet(handler, this.x + 26, this.y, 'U'));
					start = Instant.now();
				}
			}
		}
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public int getHealth() {
		return health;
	}
	public void setHealth(int health) {
		this.health = health;
	}

	@Override
	protected Rectangle bounds() {
		return new Rectangle((int) x, (int) y, 59, 42);
	}

}
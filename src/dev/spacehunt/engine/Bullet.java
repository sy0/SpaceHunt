package dev.spacehunt.engine;

import dev.spacehunt.gfx.Assets;

import java.awt.*;
import java.util.ArrayList;

public class Bullet extends Entity {
	private char direction;

	public Bullet(Handler handler, float x, float y, char direction) {
		super(handler, x, y);
		if (direction == 'U' || direction == 'D')
			this.direction = direction;
	}

	@Override
	protected void render(Graphics g) {
		if (direction == 'U')
			g.drawImage(Assets.bulletUp, (int) x, (int) y, null);
		else
			g.drawImage(Assets.bulletDown, (int) x, (int) y, null);
	}

	@Override
	protected void tick() {
		if (direction == 'U')
			y -= 3;
		else
			y += 3;
	}

	@Override
	protected Rectangle bounds() {
		return new Rectangle((int) x, (int) y, 7, 10);
	}

	public boolean checkCollision(ArrayList<Enemy> enemies) {
		for (Enemy enemy : enemies) {
			if (direction == 'U' && this.bounds().intersects(enemy.bounds())) {
				System.out.println("bam!");
				enemies.remove(enemy);
				return true;
			}
		}
		return false;
	}

	public boolean checkCollision(Ship ship) {
		if (direction != 'U') {
			if (this.bounds().intersects(ship.bounds())) {
				System.out.println("bam!");
				ship.setHealth(ship.getHealth() - 10);
				return true;
			}
		}
		return false;
	}

}

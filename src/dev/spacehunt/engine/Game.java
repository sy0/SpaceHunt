package dev.spacehunt.engine;

import dev.spacehunt.gfx.Assets;
import dev.spacehunt.gfx.Display;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;
import java.util.Random;

public class Game implements Runnable {
    private BufferStrategy bs;
    private Graphics g;
    private KeyManager keyManager;
    private Display display;
    private int width, height;
    private String title;
    private Thread thread;
    private Menu menu = new Menu();
    private boolean running = false;
    private Ship ship;
    private ArrayList<Enemy> enemies;
    private ArrayList<Bullet> bullets;
    private ArrayList<PowerUp> powerUps;
    private Random rnd = new Random();


    public static enum STATE {
        MENU,
        CREDITS,
        GAME,
        DEAD
    }

    ;
    public static STATE state = STATE.MENU;
    private Handler handler;

    private long timer = 0; // Thread's timer

    public Game(String title, int width, int height) {
        handler = new Handler(this);
        this.title = title;
        this.width = width;
        this.height = height;
        ship = new Ship(handler, 540 - (59 / 2), 700 - 42);
        enemies = new ArrayList<>();
        bullets = new ArrayList<>();
        powerUps = new ArrayList<>();
        keyManager = new KeyManager();
    }

    @Override
    public void run() {
        init();
        int fps = 60;
        double timePerTick = 1000000000 / fps;
        double delta = 0;
        long now;
        long lastTime = System.nanoTime();
        int ticks = 0;

        while (running) {
            now = System.nanoTime();
            delta += (now - lastTime) / timePerTick;
            timer += now - lastTime;
            lastTime = now;
            if (delta >= 1) {
                tick();
                render();
                delta--;
                ticks++;
            }
            if (timer >= 1000000000) {
                System.out.println(ticks);
                ticks = 0;
                timer = 0;
            }
        }
        stop();
    }

    private void tick() {

        if (state == STATE.GAME) {
            // Key listener
            keyManager.tick();
            int randomP = rnd.nextInt(2000);
            if (powerUps.size() < 1 && (randomP == 69)) {
                createRandomPowerUp();
            }
            Enemy[] willBeRemovedEnemies = new Enemy[3];
            int count = 0;
            for (Enemy enemy : enemies) {
                if (enemy != null && enemy.y >= 720 - 35) {
                    willBeRemovedEnemies[count++] = enemy;
                    ship.setHealth(ship.getHealth() - 10);
                    System.out.println("bam!");
                }
            }
            for (Enemy enemy : willBeRemovedEnemies) {
                if (enemy != null)
                    enemies.remove(enemy);
            }

            // Creating enemies
            if (enemies.size() < 3)
                enemies.add(new Enemy(handler));

            // Enemies' random movement and bullet shooting
            enemies.forEach(Enemy::randomXMovement);
            if (timer >= 1000000000) {
                enemies.forEach(Enemy::createBullets);

            }

            // Movements of all
            ship.tick();
            powerUps.forEach(PowerUp::tick);
            if (powerUps.size() != 0) {
                for (int i = 0; i < powerUps.size(); i++) {
                    boolean coll = powerUps.get(i).checkCollision(ship);
                    if (coll) {
                        powerUps.get(i).effect(ship);
                        powerUps.remove(i);
                        continue;
                    }
                    if (powerUps.get(i).y > 720) {
                        powerUps.remove(i);

                    }
                }
            }
            enemies.forEach(Enemy::tick);
            if (bullets.size() != 0) {
                bullets.forEach(Bullet::tick);
                for (int i = 0; i < bullets.size(); i++) {
                    boolean coll = bullets.get(i).checkCollision(enemies);
                    if (coll) {
                        ship.setPoint(ship.getPoint() + 10);
                        bullets.remove(bullets.get(i));
                        continue;
                    }
                    if (bullets.get(i).y > 720 || bullets.get(i).y < -1) {
                        bullets.remove(i);
                    }
                }
                for (int i = 0; i < bullets.size(); i++) {
                    boolean coll = bullets.get(i).checkCollision(ship);
                    if (coll) {
                        bullets.remove(bullets.get(i));
                    }
                }
            }
            for (int i = 0; i < enemies.size(); i++) {
                boolean coll = enemies.get(i).checkCollision(ship);
                if (coll) {
                    enemies.remove(enemies.get(i));
                    continue;
                }
                if (enemies.get(i).y > 720) {
                    enemies.remove(i);
                }
            }
            if (ship.getHealth() > 100) {
                ship.setHealth(100);
            }
            if (ship.getHealth() == 0) {
                state = STATE.DEAD;
            }
        }

    }

    private void render() {
        bs = display.getCanvas().getBufferStrategy();
        if (bs == null) {
            display.getCanvas().createBufferStrategy(3);
            return;
        }
        g = bs.getDrawGraphics();

        // Clear
        g.clearRect(0, 0, width, height);
        if (state == STATE.GAME) {
            // Draw begins
            // Ship+enemy render
            g.drawImage(Assets.background, 0, 0, null);
            g.setFont(new Font("Hack", Font.PLAIN, 32));
            g.drawString("Point : " + ship.getPoint(), 800, 40);
            g.drawString("Health : " + ship.getHealth(), 800, 680);
            ship.render(g);
            powerUps.forEach((PowerUp p) -> p.render(g));
            enemies.forEach((Enemy e) -> e.render(g));
            if (bullets.size() != 0) {
                bullets.forEach((Bullet b) -> b.render(g));
            }
        } else if (state == STATE.MENU || state == STATE.CREDITS || state == STATE.DEAD) {
            menu.render(g);
        }

        // Draw ends
        bs.show();
        g.dispose();
    }

    private void init() {
        display = new Display(title, width, height);
        display.getFrame().addKeyListener(keyManager);
        display.getCanvas().addMouseListener(new MouseManager());
        Assets.init();
    }

    public void createRandomPowerUp() {
        int typ = rnd.nextInt(3);
        int x = rnd.nextInt(1080);
        PowerUp p = new PowerUp(handler, x, 0, typ);
        powerUps.add(p);
    }

    public synchronized void start() {
        if (running)
            return;
        running = true;
        thread = new Thread(this);
        thread.start();
    }

    public synchronized void stop() {
        if (!running)
            return;
        running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public KeyManager getKeyManager() {
        return keyManager;
    }

    public Display getDisplay() {
        return display;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Ship getShip() {
        return ship;
    }

    public void setShip(Ship ship) {
        this.ship = ship;
    }

    public ArrayList<Enemy> getEnemies() {
        return enemies;
    }

    public void setEnemies(ArrayList<Enemy> enemies) {
        this.enemies = enemies;
    }

    public ArrayList<Bullet> getBullets() {
        return bullets;
    }

    public void setBullets(ArrayList<Bullet> bullets) {
        this.bullets = bullets;
    }

    public void addBullets(Bullet bullet) {
        bullets.add(bullet);
    }

    public void addPowerUps(PowerUp power) {
        powerUps.add(power);
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }
}
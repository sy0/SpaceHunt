package dev.spacehunt.gfx;

import java.awt.image.BufferedImage;

public class Assets {
    public static BufferedImage background;
    public static BufferedImage playerShip;
    public static BufferedImage enemyShip1;
    public static BufferedImage enemyShip2;
    public static BufferedImage enemyShip3;
    public static BufferedImage bulletUp;
    public static BufferedImage bulletDown;
    public static BufferedImage menu;
    public static BufferedImage healthUp;
    public static BufferedImage pointUp;
    public static BufferedImage speedUp;


    public static void init() {
        background = ImageLoader.loadImage("/images/bg_blurred.jpg");
        playerShip = ImageLoader.loadImage("/images/player.png");
        enemyShip1 = ImageLoader.loadImage("/images/enemy1.png");
        enemyShip2 = ImageLoader.loadImage("/images/enemy2.png");
        enemyShip3 = ImageLoader.loadImage("/images/enemy3.png");
        bulletUp = ImageLoader.loadImage("/images/bullet_up.png");
        bulletDown = ImageLoader.loadImage("/images/bullet_down.png");
        menu = ImageLoader.loadImage("/images/menu.jpg");
        healthUp = ImageLoader.loadImage("/images/healthUp.png");
        pointUp = ImageLoader.loadImage("/images/pointUp.png");
        speedUp = ImageLoader.loadImage("/images/speedUp.png");

    }
}
